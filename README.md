wgetCoursera
============



To download any video lecture series on Coursera

A python script to batch download video lectures of any particular course on Coursera.

Usage : python wgetFinal.py

                            
                            OR
                            

To just download videos of Startup engineering on Coursera

A python script to download videos lectures of Startup Engineering on Coursera from here(https://class.coursera.org/startup-001/lecture/index)

Usage : python wgetStartup.py

